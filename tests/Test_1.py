from src import lab1
import pytest

# Параметры для тестов на возрастание
test_param_cases_increase = [
    ([(0, 1), (1, 4), (2, 2), (3, 3)], [(0, 1), (2, 2), (3, 3), (1, 4)]),
    ([(0, 4), (1, 9), (2, 2), (3, 0), (4, 7), (5, 5), (6, 8), (7, 1), (8, 3), (9, 6)],
     [(3, 0), (7, 1), (2, 2), (8, 3), (0, 4), (5, 5), (9, 6), (4, 7), (6, 8), (1, 9)]),
    ([(0, 0), (1, 5), (2, 2), (3, 7), (4, 4), (5, 5), (6, 9), (7, 0), (8, 1)],
     [(0, 0), (7, 0), (8, 1), (2, 2), (4, 4), (1, 5), (5, 5), (3, 7), (6, 9)])
]

# Параметры для тестов на убывание
test_param_cases_decrease = [
    ([(0, 1), (1, 4), (2, 2), (3, 3)], True, [(1, 4), (3, 3), (2, 2), (0, 1)]),
    ([(0, 4), (1, 9), (2, 2), (3, 0), (4, 7), (5, 5), (6, 8), (7, 1), (8, 3), (9, 6)], True,
     [(1, 9), (6, 8), (4, 7), (9, 6), (5, 5), (0, 4), (8, 3), (2, 2), (7, 1), (3, 0)]),
    ([(0, 0), (1, 5), (2, 2), (3, 7), (4, 4), (5, 5), (6, 9), (7, 0), (8, 1)], True,
     [(6, 9), (3, 7), (1, 5), (5, 5), (4, 4), (2, 2), (8, 1), (0, 0), (7, 0)])
]

# Параметры для тестов на устойчивость (на возрастание)
test_param_cases_sustainability_increase = [
    ([(0, 5), (1, 5), (2, 5), (3, 5)], [(0, 5), (1, 5), (2, 5), (3, 5)]),
    ([(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0)], [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0)]),
    ([(0, 1), (1, 1)], [(0, 1), (1, 1)])
]

# Параметры для тестов на устойчивость (на убывание)
test_param_cases_sustainability_decrease = [
    ([(0, 5), (1, 5), (2, 5), (3, 5)], True, [(0, 5), (1, 5), (2, 5), (3, 5)]),
    ([(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0)], True, [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0)]),
    ([(0, 1), (1, 1)], True, [(0, 1), (1, 1)])
]


# Тесты пузырьковой сортировки
@pytest.mark.parametrize("x, expected", test_param_cases_increase)
def test_bubble_sort_increase(x, expected):  # на возрастание
    assert lab1.Bubble_sort(x) == expected


@pytest.mark.parametrize("x, v, expected", test_param_cases_decrease)
def test_bubble_sort_decrease(x, v, expected):  # на убывание
    assert lab1.Bubble_sort(x, v) == expected


@pytest.mark.parametrize("x, expected", test_param_cases_sustainability_increase)
def test_bubble_sort_sustainability_increase(x, expected):  # на устойчивость (на возрастание)
    assert lab1.Bubble_sort(x) == expected


@pytest.mark.parametrize("x, v, expected", test_param_cases_sustainability_decrease)
def test_bubble_sort_sustainability_decrease(x, v, expected):  # на устойчивость (на убывание)
    assert lab1.Bubble_sort(x, v) == expected


def test_bubble_sort_data():  # на корректность данных
    with pytest.raises(TypeError) as e:
        lab1.Bubble_sort(1423, True)
    assert 'the value should be a list' in str(e)


# Тесты сортировки вставками
@pytest.mark.parametrize("x, expected", test_param_cases_increase)
def test_sort_by_inserts_increase(x, expected):  # на возрастание
    assert lab1.Sort_by_inserts(x) == expected


@pytest.mark.parametrize("x, v, expected", test_param_cases_decrease)
def test_sort_by_inserts_decrease(x, v, expected):  # на убывание
    assert lab1.Sort_by_inserts(x, v) == expected


@pytest.mark.parametrize("x, expected", test_param_cases_sustainability_increase)
def test_sort_by_inserts_sustainability_increase(x, expected):  # на устойчивость (на возрастание)
    assert lab1.Bubble_sort(x) == expected


@pytest.mark.parametrize("x, v, expected", test_param_cases_sustainability_decrease)
def test_sort_by_inserts_sustainability_decrease(x, v, expected):  # на устойчивость (на убывание)
    assert lab1.Bubble_sort(x, v) == expected


def test_sort_by_inserts_data():  # на корректность данных
    with pytest.raises(TypeError) as e:
        lab1.Sort_by_inserts(1423, True)
    assert 'the value should be a list' in str(e)


# Тесты сортировки шелла
@pytest.mark.parametrize("x, expected", test_param_cases_increase)
def test_shell_sort_increase(x, expected):  # на возрастание
    assert lab1.Shell_sort(x) == expected


@pytest.mark.parametrize("x, v, expected", test_param_cases_decrease)
def test_shell_sort_decrease(x, v, expected):  # на убывание
    assert lab1.Shell_sort(x, v) == expected


@pytest.mark.parametrize("x, expected", test_param_cases_sustainability_increase)
def test_shell_sort_sustainability_increase(x, expected):  # на устойчивость (на возрастание)
    assert lab1.Bubble_sort(x) == expected


@pytest.mark.parametrize("x, v, expected", test_param_cases_sustainability_decrease)
def test_shell_sort_sustainability_decrease(x, v, expected):  # на устойчивость (на убывание)
    assert lab1.Bubble_sort(x, v) == expected


def test_shell_sort_data():  # на корректность данных
    with pytest.raises(TypeError) as e:
        lab1.Shell_sort(1423, True)
    assert 'the value should be a list' in str(e)


# Тесты быстрой сортировки
@pytest.mark.parametrize("x, expected", test_param_cases_increase)
def test_quick_sort_increase(x, expected):  # на возрастание
    assert lab1.Quick_sort(x) == expected


@pytest.mark.parametrize("x, v, expected", test_param_cases_decrease)
def test_quick_sort_decrease(x, v, expected):  # на убывание
    assert lab1.Quick_sort(x, v) == expected


@pytest.mark.parametrize("x, expected", test_param_cases_sustainability_increase)
def test_quick_sort_sustainability_increase(x, expected):  # на устойчивость (на возрастание)
    assert lab1.Bubble_sort(x) == expected


@pytest.mark.parametrize("x, v, expected", test_param_cases_sustainability_decrease)
def test_quick_sort_sustainability_decrease(x, v, expected):  # на устойчивость (на убывание)
    assert lab1.Bubble_sort(x, v) == expected


def test_quick_sort_data():  # на корректность данных
    with pytest.raises(TypeError) as e:
        lab1.Quick_sort(1423, True)
    assert 'the value should be a list' in str(e)

