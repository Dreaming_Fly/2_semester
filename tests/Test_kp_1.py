from src.kp import kp_1
import pytest

# Параметры для тестов методов прямоугольников, трапеций и Симпсона
# Значения которые должны быть получены были округлены
test_param_cases = [
    (13, 0, 100, 10, 728365875, 844791125, 842321542),
    (3, 2, 60, 20, 7160436, 7675885, 7669238),
    (6, 10, 500, 10, 12292601580, 14339910270, 14277556300)
]


# Тест интегрирования методом прямоугольников
@pytest.mark.parametrize("a, x1, x2, q, S1, S2, S3", test_param_cases)
def test_rectangle(a, x1, x2, q, S1, S2, S3):
    assert kp_1.rectangle(a, x1, x2, q) == S1


# Тест интегрирования методом трапеций
@pytest.mark.parametrize("a, x1, x2, q, S1, S2, S3", test_param_cases)
def test_trapezoid(a, x1, x2, q, S1, S2, S3):
    assert kp_1.trapezoid(a, x1, x2, q) == S2


# Тест интегрирования методом Симпсона
@pytest.mark.parametrize("a, x1, x2, q, S1, S2, S3", test_param_cases)
def test_simpson(a, x1, x2, q, S1, S2, S3):
    assert kp_1.simpson(a, x1, x2, q) == S3

