

# Функция
def function(a, x):
    return 7 * (-15 * a ** 2 + 22 * a * x + 5 * x ** 2) / 4 * a ** 2 + 7 * a * x + 3 * x ** 2


# Метод прямоугольников
def rectangle(a, x1, x2, q):
    S = 0.
    step = (x2 - x1) / q
    while True:
        S += function(a, x1) * step
        x1 += step
        if x1 + step > x2:
            return round(S + function(a, x1) * (x2 - x1))


# Метод трапеций
def trapezoid(a, x1, x2, q):
    S = 0.
    step = (x2 - x1) / q
    while True:
        S += (function(a, x1) + function(a, x1 + step)) * step / 2
        x1 += step
        if x1 + step > x2:
            return round(S + (function(a, x1) + function(a, x2)) * (x2 - x1) / 2)


# Метод Симпсона
def simpson(a, x1, x2, q):
    num_1, num_2 = 0., 0.
    S = function(a, x1) + function(a, x2)
    step = (x2 - x1) / q
    for i in range(1, q):
        if i % 2 != 0:
            num_1 += function(a, x1 + i * step)
        elif i % 2 == 0 and 2 <= i <= q - 2:
            num_2 += function(a, x1 + i * step)
    return round((2 * step / 6) * (S + 4 * num_1 + 2 * num_2))

