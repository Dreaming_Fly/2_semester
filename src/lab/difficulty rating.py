import lab1
import random
from timeit import default_timer as timer
import matplotlib.pyplot as plt
import matplotlib.ticker as tcr
from memory_profiler import memory_usage
import cProfile
import pstats
from pstats import SortKey


def main_1(m):
    memory = memory_usage((lab1.Bubble_sort, (m,),), timestamps=False, interval=1)
    return sum(memory) / len(memory)


def main_2(m):
    memory = memory_usage((lab1.Sort_by_inserts, (m,),), timestamps=False, interval=1)
    return sum(memory) / len(memory)


def main_3(m):
    memory = memory_usage((lab1.Shell_sort, (m,),), timestamps=False, interval=1)
    return sum(memory) / len(memory)


def main_4(m):
    memory = memory_usage((lab1.Quick_sort, (m,),), timestamps=False, interval=1)
    return sum(memory) / len(memory)


# создание массива с массивами рандомных чисел
mass = [[(_, random.randint(-1000, 1000)) for _ in range(i)] for i in range(1000, 10001, 1000)]

s_time_1, s_time_2, s_time_3, s_time_4 = 0, 0, 0, 0
mass_time_1, mass_time_2, mass_time_3, mass_time_4 = [], [], [], []
size_1, size_2, size_3, size_4 = 0, 0, 0, 0
mass_size_1, mass_size_2, mass_size_3, mass_size_4 = [], [], [], []
mass_qnt_num_1, mass_qnt_num_2, mass_qnt_num_3, mass_qnt_num_4 = [], [], [], []

# Заносим кол-во элементов участвующих в сортировке для построения графика
mass_qnt_in = [len(mass[_]) for _ in range(len(mass))]

# Ширина будущих столбцов в гистограммах
size_column = 1000 / 6

# Заносим кол-во элементов с небольшой корректировкой для построения гистограмм
mass_qnt_in_bar_1 = [len(mass[_]) - (size_column + size_column / 2) for _ in range(len(mass))]
mass_qnt_in_bar_2 = [len(mass[_]) - size_column / 2 for _ in range(len(mass))]
mass_qnt_in_bar_3 = [len(mass[_]) + size_column / 2 for _ in range(len(mass))]
mass_qnt_in_bar_4 = [len(mass[_]) + (size_column + size_column / 2) for _ in range(len(mass))]

if __name__ == "__main__":
    for elem in mass:
        print('Замер времени для массива из -', len(elem), 'чисел.')
        time_start_1 = timer()  # Замер времени работы
        lab1.Bubble_sort(elem)  # Пузырьковая сортировка
        mass_time_1.append(timer() - time_start_1)  # Заносим время в массив

        time_start_2 = timer()  # Замер времени работы
        lab1.Sort_by_inserts(elem)  # Сортировка вставками
        mass_time_2.append(timer() - time_start_2)  # Заносим время в массив

        time_start_3 = timer()  # Замер времени работы
        lab1.Shell_sort(elem)  # Сортировка Шелла
        mass_time_3.append(timer() - time_start_3)  # Заносим время в массив

        time_start_4 = timer()  # Замер времени работы
        lab1.Quick_sort(elem)  # Быстрая сортировка
        mass_time_4.append(timer() - time_start_4)  # Заносим время в массив

        print('Замер памяти. Это может занять какое то время.')
        mass_size_1.append(main_1(elem))  # Замер использованной памяти
        mass_size_2.append(main_2(elem))
        mass_size_3.append(main_3(elem))
        mass_size_4.append(main_4(elem))

        print('Подсчет кол-ва операций. Это может занять какое то время.')
        var = 0
        while True:
            if var == 0:
                cProfile.run('lab1.Bubble_sort(elem)', 'stats.log')
            elif var == 1:
                mass_qnt_num_1.append(int(qnt_numm[0].split()[0]))
                cProfile.run('lab1.Sort_by_inserts(elem)', 'stats.log')
            elif var == 2:
                mass_qnt_num_2.append(int(qnt_numm[0].split()[0]))
                cProfile.run('lab1.Shell_sort(elem)', 'stats.log')
            elif var == 3:
                mass_qnt_num_3.append(int(qnt_numm[0].split()[0]))
                cProfile.run('lab1.Quick_sort(elem)', 'stats.log')

            with open('output.txt', 'w') as log_file_stream:
                p = pstats.Stats('stats.log', stream=log_file_stream)
                p.strip_dirs().sort_stats(SortKey.NAME == 'swap').print_stats()

            with open('output.txt') as f:
                lines = [line.strip() for line in f]

            qnt_numm = [line for line in lines if line not in '' and line.count('swap')]

            var += 1

            if var == 4:
                mass_qnt_num_4.append(int(qnt_numm[0].split()[0]))
                break
        print('---')

    print('Построение графика. Это может занять какое то время.')

    # График времени
    plt.figure(figsize=(18, 6))
    ax = plt.subplot(131)
    plt.plot(mass_qnt_in, mass_time_1, marker='o', ms=3, label='Пузырьковая сортировка')
    plt.plot(mass_qnt_in, mass_time_2, marker='o', ms=3, label='Сортировка по вставкам')
    plt.plot(mass_qnt_in, mass_time_3, marker='o', ms=3, label='Сортировка Шелла')
    plt.plot(mass_qnt_in, mass_time_4, marker='o', ms=3, label='Быстрая сортировка')
    plt.title('Времени')
    plt.xlabel('Кол-во чисел')
    plt.ylabel('Время (с)')
    plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Виды сортировок', title_fontsize='8')
    ax.set_xlim(mass_qnt_in[0], mass_qnt_in[-1])
    ax.xaxis.set_major_locator(tcr.MultipleLocator(2000))
    plt.grid()

    # График кол-ва операций
    ax = plt.subplot(132)
    plt.plot(mass_qnt_in, mass_qnt_num_1, marker='o', ms=3)
    plt.plot(mass_qnt_in, mass_qnt_num_2, marker='o', ms=3)
    plt.plot(mass_qnt_in, mass_qnt_num_3, marker='o', ms=3)
    plt.plot(mass_qnt_in, mass_qnt_num_4, marker='o', ms=3)
    plt.title('Кол-ва операций')
    plt.xlabel('Кол-во чисел')
    plt.ylabel('Кол-во операций')
    ax.xaxis.set_major_locator(tcr.MultipleLocator(2000))
    plt.grid()

    # График памяти
    ax = plt.subplot(133)
    plt.bar(mass_qnt_in_bar_1, mass_size_1, width=size_column)
    plt.bar(mass_qnt_in_bar_2, mass_size_2, width=size_column)
    plt.bar(mass_qnt_in_bar_3, mass_size_3, width=size_column)
    plt.bar(mass_qnt_in_bar_4, mass_size_4, width=size_column)
    plt.title('Памяти')
    plt.xlabel('Кол-во чисел')
    plt.ylabel('Память (байт)')
    ax.xaxis.set_major_locator(tcr.MultipleLocator(2000))
    plt.grid()

    plt.show()  # Вывод графика

